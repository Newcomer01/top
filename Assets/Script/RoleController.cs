using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoleController : MonoBehaviour
{
    public UIMain uI;
    //操作方式
    public enum ControlTypeTest
    {
        mouseControl,
        touchControl,
    }
    public ControlTypeTest controlType;
    public Transform rotTarget;

    //旋转速度加成系数
    public float rotSpeedScalar = 20f;//角速度
    private float currentSpeed = 0;//旋转速度

    private void Start()
    {
       
    }
    void Update()
    {
        if (controlType == ControlTypeTest.mouseControl)
        {
            //鼠标操作
            if (Input.GetMouseButton(0))
            {
                //拖动时速度
                //随着时间进行速度限制,使速度进行线性插值
                currentSpeed = Mathf.Lerp(currentSpeed, Input.GetAxis("Mouse X") / Time.deltaTime * 5, 1.2f * Time.deltaTime);
            }
            else
            {
                //放开时速度逐渐减小
                currentSpeed = Mathf.Lerp(currentSpeed, 0, 1f * Time.deltaTime);
               
            }
        }
        else if (controlType == ControlTypeTest.touchControl)
        {

            //触摸操作
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                
                currentSpeed = Mathf.Lerp(currentSpeed, Input.touches[0].deltaPosition.x / Time.deltaTime / 10, 1.2f * Time.deltaTime);
               
            }
            else
            {
                currentSpeed = Mathf.Lerp(currentSpeed, 0, 1f * Time.deltaTime);
               
            }
        }

        //物体旋转朝向以及旋转的条件矢量
        rotTarget.Rotate(Vector3.forward, Time.deltaTime * currentSpeed * rotSpeedScalar);

       //文字显示，取整并取绝对值
        uI.txtSpeed.text =Mathf.Abs(Mathf.Round(currentSpeed)).ToString() + "/" + "min";
        
    }
}
